package aoc.y2019

import scala.io.Source
import Day09._

object Day25 {

  def executeIntCode01(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = scala.collection.mutable.Map[BigInt,BigInt]()
    codesTuples.foreach{case (v,i) => codes(i) = v}


    val state = State(codes = codes.clone)

    runDroid("", state)
  }

  def ascii(s: String):List[BigInt] = {
    if(s.length > 0){
      s.toList.map(ch => BigInt(ch.toInt)) ++ List(BigInt(10))
    }else{
      Nil
    }
  }

  def parseOutput(out: List[BigInt]): String = {
    out.foldLeft(""){case (s,i) => s + i.toByte.toChar}
  }

  //TODO: Only started with the code.
  def runDroid(cmd: String, state: State): Unit = {
    if(cmd == "exit!"){
      return
    }else{
      val inp = ascii(cmd)
      val rs = interprete(state.copy(output = Nil).copy(inputs = inp))
      println(parseOutput(rs.output.reverse))
      val newCommand = ">"
      println("You entered : " + newCommand)
      runDroid(newCommand,rs)
    }
  }
}
//- weather machine
//- shell
//- candy cane
//- whirled peas
//- hypercube
//- space law space brochure
//- space heater
//- spool of cat6
