package aoc.y2019

import scala.io.Source
import aoc.y2019.Day09._
import scala.collection.immutable.Queue

object Day15 {

  type Position = (Int,Int)

  trait PType
  case class Wall() extends PType
  case class Empty() extends PType
  case class Found() extends PType
  object PType{
    val from: Int => PType = {
      case 0 => Wall()
      case 1 => Empty()
      case 2 => Found()
    }
  }
  trait Direction{val code: Int}
  case class North() extends Direction{val code = 1}
  case class South() extends Direction{val code = 2}
  case class West() extends Direction{val code = 3}
  case class East() extends Direction{val code = 4}

  object Direction{
    val allDirections = List[Direction](North(), South(), West(), East())
    val go:(Direction,Position) => Position = {
      case (East(), (x,y)) => (x+1,y)
      case (West(), (x,y)) => (x-1,y)
      case (South(), (x,y)) => (x,y+1)
      case (North(), (x,y)) => (x,y-1)
    }
  }
  import Direction._

  def executeIntCode(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = scala.collection.mutable.Map[BigInt,BigInt]()

    codesTuples.foreach{case (v,i) => codes(i) = v}

    val state = State(codes=codes)

    val rs = find(Queue((0 -> 0) -> state), Map((0 -> 0) -> Some(0)), None, None)
    println(rs._2)
  }

  def check(state: State, direction: Direction): (State, PType) = {
    val copyState = state.copy(codes = state.codes.clone).copy(inputs = List(direction.code)).copy(output = List())
    val newState = interprete(copyState)
    //println("Output : " + newState.output)
    val out = newState.output.head.toInt
    (newState, PType.from(out))
  }
  
  def find(queue: Queue[(Position, State)], visited: Map[Position, Option[Int]], cylinderDist: Option[Int], cylinderPos: Option[Position]):
      (Map[Position, Option[Int]], Option[Int], Option[Position]) = {
    if(queue.isEmpty || visited.size > 1000000){
      return (visited, cylinderDist, cylinderPos)
    }else{
      val ((currentPos, currentState),newqueue) = queue.dequeue
      val currentDistance = visited.get(currentPos).get.get

      //filter the already visited positions from the positions reachable from current Position
      val filteredPos:List[(Direction,Position)] =
        allDirections.map(drctn => (drctn, go(drctn,currentPos))).filter{
          case (drctn, position) => !visited.contains(position)
        }

      //check if these filtered positions can be visited
      val possibleVisits:List[(Direction, Position, State, PType)] = filteredPos.map{
        case (drctn, position) =>
          val chk = check(currentState,drctn)
          (drctn, position, chk._1, chk._2)
      }

      val (nqueue, nvisited, foundDist, foundPos) =
        possibleVisits.foldLeft(newqueue, visited, cylinderDist, cylinderPos: Option[Position]){
          case ((nqueue,nvisited,foundDist,foundPos), (_, newPos, state, Empty())) =>
            (nqueue.enqueue(newPos -> state), (nvisited + (newPos -> Some(currentDistance + 1))), foundDist, foundPos)
          case ((nqueue,nvisited,foundDist,foundPos), (_, newPos, _, Wall())) => (nqueue, (nvisited + (newPos -> None)), foundDist, foundPos)
          case ((nqueue,nvisited,foundDist,foundPos), (_, newPos, state, Found())) =>
            (nqueue.enqueue(newPos -> state), (nvisited + (newPos -> Some(currentDistance + 1))), Some(currentDistance + 1), Some(newPos))
        }

      find(nqueue, nvisited, foundDist, foundPos)
    }
  }

  def executeIntCode02(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = scala.collection.mutable.Map[BigInt,BigInt]()

    codesTuples.foreach{case (v,i) => codes(i) = v}

    val state = State(codes=codes)

    val rs = find(Queue((0 -> 0) -> state), Map((0 -> 0) -> Some(0)), None, None)
    println(rs._2)

    val dist = findTime(Queue(rs._3.get), Map(rs._3.get -> 0), rs._1)
    println(dist)
  }

  def findTime(queue: Queue[Position], visited: Map[Position, Int], all: Map[Position, Option[Int]]): Int = {
    if(queue.isEmpty){
      return visited.values.toList.max
    }else{
      val (currentPos, newqueue) = queue.dequeue
      val currentTime = visited.get(currentPos).get

      //filter the already visited positions from the positions reachable from current Position
      val filteredPos:List[Position] =
        allDirections.map(go(_,currentPos)).filter{
          case position => !visited.contains(position) && all.contains(position) && all.get(position).get.nonEmpty  
        }

      val newVisited = visited ++ filteredPos.map((_, currentTime + 1))

      val rsQueue:Queue[Position] = filteredPos.foldLeft(newqueue){
        case (rsQueue, position) => rsQueue.enqueue(position)
      }

      findTime(rsQueue, newVisited, all)

    }
  }
  
}
