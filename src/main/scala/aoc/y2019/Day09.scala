package aoc.y2019

import io.Source
import scala.collection.Set
import scala.collection.mutable.Map

object Day09 {

  trait OpCode
  case class In() extends OpCode
  case class Out() extends OpCode
  case class Add() extends OpCode
  case class Mul() extends OpCode
  case class JiT() extends OpCode
  case class JiF() extends OpCode
  case class LessT() extends OpCode
  case class EQL() extends OpCode
  case class RBase() extends OpCode
  case class Halt() extends OpCode

  trait Mode
  case class Position() extends Mode
  case class Relative() extends Mode
  case class Immediate() extends Mode

  type Operation = (OpCode,Mode,Mode,Mode)

  def operation(v: BigInt): Operation = {
    val s = v.toString.toList.reverse
    val op:OpCode = s.take(2) match {
      case ('1'::_) => Add()
      case ('2'::_) => Mul()
      case ('3'::_) => In()
      case ('4'::_) => Out()
      case ('5'::_) => JiT()
      case ('6'::_) => JiF()
      case ('7'::_) => LessT()
      case ('8'::_) => EQL()
      case ('9'::'9'::_) => Halt()
      case ('9'::_) => RBase()
    }
    val params = s.drop(2)
    def mode(i: Int): Mode = {
      if (i < params.length){
        params(i) match {
          case '0' => Position()
          case '1' => Immediate()
          case '2' => Relative()
        }
      }else{
        Position()
      }
    }
    (op,mode(0),mode(1),mode(2))
  }

  def readByMode(m: Mode, p: BigInt, codes: Map[BigInt,BigInt], base: BigInt): BigInt = {
    if (m == Position() && p < 0){
      throw new Error("Position mode Negative Address : " + p)
    }
    if (m == Relative() && (base + p) < 0){
      throw new Error("Relative Negative Address : " + p + " base " + base)
    }
    m match {
    case Immediate() => p
    case Position() => codes.getOrElse(p,0)
    case Relative() => codes.getOrElse(base+p,0)
    }
  }

  def writeByMode(m: Mode, p: BigInt, value: BigInt, codes: Map[BigInt,BigInt], base: BigInt): Unit = {
    if(m == Immediate()){
      throw new Error()
    }else{
      m match {
        case Position() => codes(p) = value
        case Relative() => codes(base+p) = value
      }
    }
  }

  case class State(base: BigInt=0, pc: BigInt=0, codes: Map[BigInt,BigInt], inputs: List[BigInt]=List(), output: List[BigInt]=List(), stopped: Boolean=false)

  def interprete(state: State): State = {
    var base:BigInt = state.base
    var pc:BigInt = state.pc
    var output = state.output
    val codes = state.codes
    var ip = 0
    var read = false
    while(true){
      if(state.stopped){
        println("Halted!")
        return state
      }
      val opr@(op,m1,m2,m3) = operation(codes(pc))
      // println("Executing " + op + " at " + pc)
      // println("Current Codes " + codes)
      // println()
      // println()
      op match {
        case Add() =>
          val v1 = readByMode(m1,codes.getOrElse(pc+1,0),codes,base)
          val v2 = readByMode(m2,codes.getOrElse(pc+2,0),codes,base)
          writeByMode(m3,codes(pc+3),v1+v2,codes,base)
          pc += 4
        case Mul() =>
          val v1 = readByMode(m1,codes.getOrElse(pc+1,0),codes,base)
          val v2 = readByMode(m2,codes.getOrElse(pc+2,0),codes,base)
          writeByMode(m3,codes(pc+3),v1*v2,codes,base)
          pc += 4
        case LessT() =>
          val v1 = readByMode(m1,codes.getOrElse(pc+1,0),codes,base)
          val v2 = readByMode(m2,codes.getOrElse(pc+2,0),codes,base)
          writeByMode(m3,codes(pc+3),if (v1 < v2) 1 else 0,codes,base)
          pc += 4
        case EQL() =>
          val v1 = readByMode(m1,codes.getOrElse(pc+1,0),codes,base)
          val v2 = readByMode(m2,codes.getOrElse(pc+2,0),codes,base)
          writeByMode(m3,codes(pc+3),if (v1 == v2) 1 else 0,codes,base)
          pc += 4
        case JiT() =>
          val v1 = readByMode(m1,codes.getOrElse(pc+1,0),codes,base)
          if(v1 != 0){
            pc = readByMode(m2,codes.getOrElse(pc+2,0),codes,base)
          }else {
            pc += 3
          }
        case JiF() =>
          val v1 = readByMode(m1,codes.getOrElse(pc+1,0),codes,base)
          if(v1 == 0){
            pc = readByMode(m2,codes.getOrElse(pc+2,0),codes,base)
          }else {
            pc += 3
          }
        case In() =>
          if(ip >= state.inputs.length){
            return State(base=base, pc=pc,codes=codes,output=output,stopped=state.stopped)
          }else{
            writeByMode(m1,codes.getOrElse(pc+1,0),state.inputs(ip),codes,base)
            ip += 1
            pc += 2
            read = true
          }
        case Out() =>
          val v1 = readByMode(m1,codes.getOrElse(pc+1,0),codes,base)
          output = v1 :: output
          pc += 2
        case RBase() =>
          val v1 = readByMode(m1,codes.getOrElse(pc+1,0),codes,base)
          base += v1
          pc += 2
        case Halt() =>
          pc += 1
          return State(base = base, pc=pc,codes=codes,output=output,stopped=true)
      }
      if (pc >= codes.size){
        println("pc exceeding the number of codes!!")
        return State(base = base, pc=pc,codes=codes,output=output,stopped=true)
      }
    }
    return State(base = base, pc=pc,codes=codes,output=output,stopped=true)
  }

  def show(arr: Array[Int]): String = {
    var s = new String()
    arr.map(i => s = s + "," + i)
    s
  }

  val test1 = """109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"""
  val test2 = """1102,34915192,34915192,7,4,7,99,0"""
  val test3 = """104,1125899906842624,99"""

  def test(v: String): Unit = {
    val codesTuples = v.split(',').map(BigInt(_)).zipWithIndex

    val codes = Map[BigInt,BigInt]()

    codesTuples.foreach{case (v,i) => codes(i) = v}

    println(codes)

    val rs = interprete(State(codes=codes, inputs=List(1)))

    println(rs)
  }

  def execute(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = Map[BigInt,BigInt]()

    codesTuples.foreach{case (v,i) => codes(i) = v}

    val rs = interprete(State(codes=codes, inputs=List(2)))

    println(rs.output)
  }
}
