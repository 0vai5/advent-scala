package aoc.y2019

import scala.io.Source
import Day09._

object Day17 {

  type Position = (Int,Int)

  type Element = Either[Char, Int]

  val mapper: (Char => Element) = {
    case 'R' => Left('R')
    case 'L' => Left('L')
    case x => Right(x.toInt - 48)
  }

  def executeIntCode(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = scala.collection.mutable.Map[BigInt,BigInt]()

    codesTuples.foreach{case (v,i) => codes(i) = v}

    val state = State(codes=codes.clone)

    val rs = interprete(state).output.reverse

    for (s <- rs){
      print(s.toChar)
    }

    val positionsMap: Map[Position,Char] =
      rs.scanLeft((-1,0),10.toChar){
        case (((x,y),_), code) =>
          if (code.toByte == 10){
            ((-1,y+1), code.toChar)
          }else {
            ((x+1,y), code.toChar)
          }
      }.toMap

    val robot = positionsMap.find{
      case ((x,y), code) => List('^', '<', '>', 'v').contains(code)
    }.get._1
    println("Robot " + robot)

    //Uncomment for part1
    //val intersects = findIntersections(positionsMap, List(robot), Set(robot), List())
    //println(intersects)
    //println(intersects.size)
    //println(intersects.foldLeft(0){case (s, (a,b)) => s + (a*b)})

    //skip 0 steps if any from the beginning
    val thePath:List[Element] = path(positionsMap, robot, None, Up(), 0)

    val rsABC = findABC(thePath, 2, 10).head
    println()
    println()
    println(rsABC)

    val mainF = ascii(rsABC.path)
    val funcA = ascii(rsABC.A)
    val funcB = ascii(rsABC.B)
    val funcC = ascii(rsABC.C)
    val yn = List(BigInt('n'.toByte.toInt), BigInt(10))

    val part2Input:List[BigInt] = mainF ++ funcA ++ funcB ++ funcC ++ yn

    val modifiedCode = codes.clone
    modifiedCode(0) = 2

    val part2State = State(codes=modifiedCode, inputs=part2Input)
    val part2rs = interprete(part2State)
    println("Is Stopped: " + part2rs.stopped)
    println("Output size : " + part2rs.output.size)
    //println(part2rs.output.reverse)
    //it seems that incode is printing a lot of output
    //the last output is the result
    //to get last simply don't reverse and get first
    //since Day09 code writtedn returns reverse output
    println(part2rs.output.head)
  }

  def redditExample(): Unit = {
    //Taken from https://www.reddit.com/r/adventofcode/comments/ebz338/2019_day_17_part_2_pathological_pathfinding/
    //the intcode program for this is also mentioned in above reddit - https://gist.githubusercontent.com/Voltara/2de071eac63629e82b7194ab78e56546/raw/d9617d750d8b16f85049a6f87a307e6753dcc4c7/day17tricky.intcode
    val path = "R,10,L,6,L,10,R,10,R,6,R,6,L,10,L,4,L,4,R,6,R,6,L,10,L,4,L,4,R,6,R,6,L,10,L,14,L,6,L,10,R,12,L,10,R,6,R,12,L,4,R,6,R,6,L,10,L,4,L,6,L,10,R,6,R,22,L,6,L,10,R,12,L,10,R,6,R,12,R,10,L,6,L,10,R,6,4,R,6,R,6,L,10,L,4,L,4,R,6,R,6,L,10,L,4,L,4,R,6,R,6,L,10,L,4,L,R,10,L,6,L,10,R,6,6,L10,R6,R12,L,4,R,6,R,6,L,10,L,4,L,6,L,10,R,6,R,12,L,R,10,L,6,L,10,R,6,6,L,10,R,6,R,12,L"
    //my code won't work bec of two cases:
    //1 - see R,6,6 instead of R,12
    //2 - and see L at the end
    //for case 1 - perhaps i should generate paths only with 1 step. like R,6-> R,1,1,1,1,1,1 - then merge the resulting A,B,C and take the
    //shortest(or <20 rule) after this merge
    //for case 2 - add 'one' extra L or R or nothing at the end bec it doesn't make any change in path. Two L,L won't be needed bec we never
    //take two turns continously without a step
  }

  case class ABCPath(
    val A:List[Element],
    val B:List[Element],
    val C:List[Element],
    val path:List[Element] // this contains path using A,B,C
  )

  //prefixes is a list named with Char keys like 'A' -> List[Element]
  //returns the remaining list for each prefix if the list starts with that prefix
  def startsWithOr[A](list: List[A], prefixes: List[(Char,List[A])]): List[(Char, List[A])] = {
    val (emptyPrefixes, nonEmptyPrefixes) = prefixes.partition{
      case (_, Nil) => true
      case _ => false
    }
    val remnListAfterPrefixes =
      emptyPrefixes.map{case (ch, _) => (ch, list)}
    if(list.isEmpty){
      return remnListAfterPrefixes
    }else {
      val remnPrefixes =
        for {
          (code,prefix) <- nonEmptyPrefixes
          if (prefix.head == list.head)
        } yield (code, prefix.tail)
        remnListAfterPrefixes ++ startsWithOr(list.tail, remnPrefixes)
    }
  }

  def ascii(list: List[Element]):List[BigInt] = list match {
    case List() => List(BigInt(10))
    case Left(ch)::rem => BigInt(ch.toByte.toInt)::(if(rem.nonEmpty) BigInt(','.toByte.toInt)::ascii(rem) else ascii(rem))
    case Right(i)::rem => BigInt(i+48)::(if(rem.nonEmpty) BigInt(','.toByte.toInt)::ascii(rem) else ascii(rem))
  }

  def printElements(list: List[Element]):Unit = {
    list.foreach{
      case Left(x) => print(x); print(", ")
      case Right(x) => print(x); print(", ")
    }
  }

  //it returns the paths starting with the given prefixes. the second tuple is the unmatched part.
  //say we passed 'list' and p1,p2,p3 as prefixes. then 'each' data in the result returned will contain two parts
  //the eg: [p1,p2,p1,p1,p3](or [p1,p2,p3]*) and the remaning unmatched part in the 'list' passed.
  def paths(remn: List[Element], prefixes: List[(Char, List[Element])]): List[(List[Element], List[Element])] = {
    if(remn.isEmpty){
      List()
    }else{
      val remnAfterPrefixes = startsWithOr(remn, prefixes)
      if(remnAfterPrefixes.isEmpty){
        List((List(),remn))
      }else{
        remnAfterPrefixes.map{
          case (prefixName, remnAfterPrefix) =>
            if (remnAfterPrefix.isEmpty){
              List((List(Left(prefixName)), List()))
            }else {
              val remnPaths = paths(remnAfterPrefix, prefixes)
              remnPaths.map{case (remnPath,remnList) => (Left(prefixName)::remnPath, remnList)}
            }
        }.flatten
      }
    }
  }

  def printAndReturn(name: String, list: List[Element]): List[Element] = {
    println("Name : " + name)
    printElements(list)
    println()
    List(Right(1000))
  }

  def findABC(path: List[Element], min: Int, max: Int): List[ABCPath] = {
    val totalLength = path.length

    def check(a: Int, b: Int, c: Int): List[ABCPath] = {
      if (a+b+c > totalLength){
        println("more than length", a, b,c,totalLength)
        return List()
      }else {
        for{
          prefixA <- List(path.take(a))
          (pathWithA,remnAfterA) <- paths(path, List(('A',prefixA)))
          prefixB <- List(remnAfterA.take(b))
          (pathWithAB,remnAfterAB) <- paths(remnAfterA, List(('A', prefixA), ('B', prefixB)))
          prefixC <- List(remnAfterAB.take(c))
          (pathWithABC,remnAfterABC) <- paths(remnAfterAB, List(('A', prefixA), ('B', prefixB), ('C', prefixC)))
          if(remnAfterABC.isEmpty)
          //if(possiblePath.length <= 10)
        } yield ABCPath(prefixA, prefixB, prefixC, pathWithA ++ pathWithAB ++ pathWithABC)
      }
    }

    val ns: List[Int] = (min to max).toList
    for{
      lnA <- ns
      lnB <- ns
      lnC <- ns
      abcPath <- check(lnA, lnB, lnC)
    } yield (abcPath)  
  }

  //part1 function
  def findIntersections(positionsMap: Map[Position,Char], toVisit: List[Position], visited: Set[Position], intersections: List[Position]): List[Position] = {
    if(toVisit.isEmpty){
      return intersections
    }else{
      val curr@(cx,cy) = toVisit.head

      val neighbours =
        List((cx-1, cy), (cx+1, cy), (cx, cy-1), (cx, cy+1)).
          filter(positionsMap.getOrElse(_, ' ') == '#')

      val newToVisit = neighbours.filter(!visited.contains(_)).foldLeft(toVisit.tail){
        case (tv, nv) => nv::tv
      }

      val newIntersections =
        if(neighbours.size == 4){
          curr::intersections
        }else {
          intersections
        }

      val newVisited = visited ++ newToVisit

      findIntersections(positionsMap, newToVisit, newVisited, newIntersections)
    }
  }

  trait Direction{
    def delta: Position
    def left: Direction
    def right: Direction
  }
  case class LT() extends Direction{
    val delta = (-1,0)
    def left = Down()
    def right = Up()
  }
  case class RT() extends Direction{
    val delta = (1,0)
    def left = Up()
    def right = Down()
  }
  case class Up() extends Direction{
    val delta = (0,-1)
    def left = LT()
    def right = RT()
  }
  case class Down() extends Direction{
    val delta = (0,1)
    def left = RT()
    def right = LT()
  }
  object Direction {
    def from(code: Char): Direction = code match {
      case '^' => Up()
      case 'v' => Down()
      case '>' => RT()
      case '<' => LT()
    }
    def step(dir: Direction, pos: Position): Position = {
      val (x,y) = pos
      val (dx,dy) = dir.delta
      (x+dx, y+dy)
    }
    def directions = List(LT(), RT(), Up(), Down())
  }

  type Turn = Option[Direction]

  //Turn is the last "turn" robot took which can only be left or right
  //Direction is the current direction/facing of the robot
  def path(positionsMap: Map[Position,Char], pos: Position, turn: Turn, dir: Direction, steps: Int): List[Element] = {
    import Direction._
    //println(pos, turn, dir, steps)
    val possibilities: List[(Turn, Direction, Position)] =
      List((turn, dir, step(dir,pos)),
        (Some(LT()), dir.left, step(dir.left, pos)),
        (Some(RT()), dir.right, step(dir.right, pos)))

    val next: Option[(Turn,Direction,Position)] =
      possibilities.find{case (t,d,p) => positionsMap.getOrElse(p,' ') == '#'}

    //a step should not be more than 9 since intcode needs ascii codes
    //so if step is 12 break it to 9,3
    def makeASCIISteps(s: Int): List[Element] = {
      if(s <= 0){
        List()
      }else if (s > 9){
        Right(9)::makeASCIISteps(s - 9)
      }else {
        Right(s)::List()
      }
    }

    def makeElement = turn match {
      case None => makeASCIISteps(steps)
      case Some(LT()) => Left('L')::makeASCIISteps(steps)
      case Some(RT()) => Left('R')::makeASCIISteps(steps)
    }

    next match {
      //not able to move forward,left or right - or end is reached
      case None => makeElement
      case Some((nturn, ndir, npos)) =>
        //if turn is not taken
        if(ndir == dir){
          path(positionsMap, npos, nturn, ndir, steps+1)
        }else{
          makeElement ++ path(positionsMap, npos, nturn, ndir, steps = 1)
        }
    }
  }

}
