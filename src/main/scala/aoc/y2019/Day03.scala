package aoc.y2019

import scala.io.Source

object Day03 {

  def parsePoints(input: String): List[(Int,Int)] = {
    def parsePoint(refPoint: (Int,Int), s: String): ((Int,Int),IndexedSeq[(Int, Int)]) = {
      val d = s.substring(1).toInt
      val (refx,refy) = refPoint
      s(0) match {
        case 'R' => ((refx + d, refy), (refx to (refx + d)).map((_, refy)).tail)
        case 'L' => ((refx - d, refy), ((refx - d) to refx).map((_, refy)).reverse.tail)
        case 'U' => ((refx, refy + d), (refy to (refy + d)).map((refx, _)).tail)
        case 'D' => ((refx, refy - d), ((refy - d) to refy).map((refx, _)).reverse.tail)
      }
    }
    input.split(',').foldLeft(((0,0),List[(Int,Int)]((0,0))))((rs, str) => {
      val (oldRefPoint, accumulatedPoints) = rs
      val (newRefPoint, newPoints) = parsePoint(oldRefPoint, str)
      (newRefPoint, accumulatedPoints ++ newPoints)
    })._2
  }

  def solve01(s1: String, s2: String): Int = {
    val w1 = parsePoints(s1)
    val w2 = parsePoints(s2)
    val common = w1.intersect(w2).filter(p => !(p._1 == 0 && p._2 == 0))
    import Math.abs
    import Math.min
    common.foldLeft(Int.MaxValue)((rs, point) => min(rs, abs(point._1) + abs(point._2)))
  }

  def part01(fname: String): Int = {
    val lines = Source.fromFile(fname).getLines().toList
    solve01(lines(0), lines(1))
  }

  def solve02(s1: String, s2: String): Int = {
    val w1 = parsePoints(s1)
    val w2 = parsePoints(s2)
    val common = w1.intersect(w2).filter(p => !(p._1 == 0 && p._2 == 0))

    common.map(point =>
      w1.indexOf(point) + w2.indexOf(point)
    ).foldLeft(Int.MaxValue)(Math.min)
  }

  def part02(fname: String): Int = {
    val lines = Source.fromFile(fname).getLines().toList
    solve02(lines(0), lines(1))
  }
}
