package aoc.y2019

object Day04 {

  def check01(s: String): Boolean = {
    !s.zip(s.tail).exists{case (a,b) => a > b} &&
    s.zip(s.tail).exists{case (a,b) => a == b}
  }

  def test01(s: Int, e: Int): Int = {
    (s to e).filter{i:Int => check01(i.toString)}.length
  }

  def check02(s: String): Boolean = {
    !s.zip(s.tail).exists{case (a,b) => a > b} &&
    s.groupBy(c => c).values.exists{case l => l.length == 2}
  }

  def test02(s: Int, e: Int): Int = {
    (s to e).filter{i:Int => check02(i.toString)}.length
  }

}
