package aoc.y2019

import scala.io.Source

import Day09._

object Day19 {

  type IntCode = (Int,Int) => Int

  def executeIntCode(fname: String, sz: Int = 50): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = scala.collection.mutable.Map[BigInt,BigInt]()
    codesTuples.foreach{case (v,i) => codes(i) = v}

    val fun: IntCode = intCode(codes)

    val output:List[Int] =
      for{
        i <- (0 to sz-1).toList
        j <- (0 to sz-1).toList
        val rs = fun(j,i)
      } yield rs
    
    for(i <- 0 to sz-1){
      println()
      for(j <- 0 to sz-1){
        print(output(i*sz +j))
      }
    }
    println()
    println("Part 1 : " + output.sum)

    println()
    val sqrSize = 100
    val rs@(rsx,rsy) = zigZag(fun)(sqrSize)

    val part2Rs = 10000*(rsx - (sqrSize-1)) + rsy
    println("Part 2 : " + part2Rs)

  }

  def intCode(codes: scala.collection.mutable.Map[BigInt,BigInt])(x: Int, y: Int): Int = {
    val rs = interprete(State(codes=codes.clone, inputs=List(x,y))).output.head
    rs.toInt
  }

  //the ray is two diverting lines. we follow the upper line in the ray to search for required point/square
  def zigZag(func: IntCode)(sqrSize: Int): (Int, Int) = {
    //returns next column in the given row with the given value
    //searches from left to right
    //there are some empty rows in beginning
    //for that reason using maxAttempts. if attempts expires check in next row using the original col
    val maxAttempts = 4
    def nextColInRow(col: Int, row: Int, value: Int, tries: Int = maxAttempts): (Int,Int) = {
      if(tries > 0){
        if(func(col,row) == value){
          (col,row)
        }else{
          nextColInRow(col+1,row,value, tries-1)
        }
      }else{
        //println("Attempts failed" + col + " " + row + " looking for " + value)
        nextColInRow(col-maxAttempts,row+1,value)
      }
    }
    //check if sqr is possible from the given the top right corner
    def check(x: Int, y: Int): Boolean = {
      //println("Checking at : " + x + " , " + y)
      val bx = x - (sqrSize-1)
      val by = y + (sqrSize-1)
      if(bx >= 0){
        func(bx,by) == 1
      }else{
        false
      }
    }
    //the ray has two diverting lines. it assumes that the point passed is the upper/topmost point of the ray
    //thus the value at this point must be 1
    def find(col: Int, row: Int): (Int,Int) = {
      if(check(col,row)){
        (col,row)
      }else{
        val (firstColWithOne, rw) = nextColInRow(col, row + 1, 1)
        val (firstColWithZero, rww) = nextColInRow(firstColWithOne, rw, 0)
        find(firstColWithZero - 1, rww)
      }
    }

    val (sx,sy) = (0,0)
    //normally need to start from 0,0 but for the case if req. square is present at the origin itself!

    val (nx, ny) = nextColInRow(sx, sy, 0)
    find(nx - 1, ny)
  }

}
