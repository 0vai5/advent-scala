package aoc.y2019

import scala.io.Source

import Day09._


//Part-1 comments
//I took too long to understand this question!
//can't make out what even jump means even after checking the example
//i though jumping means one step up and right. well eg was showing the whole process of jumping
//so after jump finishes the springbot moves 4 tiles ahead effectively.
//springbot is moving on the hull and we need to jump if there is a hole that we can see.
//read the comments on how i tried different inputs
//it kind of appeared a brute force approach

object Day21 {

  def executeIntCode02(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = scala.collection.mutable.Map[BigInt,BigInt]()
    codesTuples.foreach{case (v,i) => codes(i) = v}

    //this is again a trial and error thing
    //it was not working using solution of previous part. case was that it should 'not' jump when E 'and' H are empty. thus it should jump if either E or H are True/ground/not-empty - reverse of (E AND H BOTH empty)
    //not to add "and (E or H) in the existing condition i need to know the value of T. used the trick that (not C and C) = True
    val inputs: List[String] = List("NOT A J", "NOT B T", "OR T J", "NOT C T", "OR T J", "AND D J", "AND C T", "OR E T", "OR H T", "AND T J","RUN")
    val state = State(codes=codes.clone, inputs = inputs.map(ascii).flatten)
    val rs = interprete(state).output.reverse

    println(rs)
    println(parseOutput(rs))

  }

  def executeIntCode01(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = scala.collection.mutable.Map[BigInt,BigInt]()
    codesTuples.foreach{case (v,i) => codes(i) = v}

    //val inputs: List[String] = List("NOT A J", "NOT B T", "AND T J", "NOT C T", "AND T J", "AND D J", "WALK")
    //val inputs = List("OR  A T", "AND B T", "AND C T", "NOT T J", "AND D J", "WALK")

    //super greedy to jump - jump if possible always
    //val inputs: List[String] = List("OR D J", "WALK") -> D = True then J = True

    //super conservative to jump - if only not able to move next step -> if next step is not ground then jump
    //val inputs: List[String] = List("NOT A T", "OR T J", "WALK")

    //A = False and D = True then only jump
    //val inputs: List[String] = List("NOT A T", "AND D T", "OR T J", "WALK")

    //B = False and D = True then only jump
    //val inputs: List[String] = List("NOT B T", "AND D T", "OR T J", "WALK")

    //either A or B is False and D = True then only jump
    //val inputs: List[String] = List("NOT A J", "NOT B T", "OR T J", "AND D J", "WALK")

    //either A or B or C is False and D = True then only jump
    //well this solution worked but this doesn't mean it will work for every input
    val inputs: List[String] = List("NOT A J", "NOT B T", "OR T J", "NOT C T", "OR T J", "AND D J", "WALK")

    val state = State(codes=codes.clone, inputs = inputs.map(ascii).flatten)
    val rs = interprete(state).output.reverse

    println(rs)
    println(parseOutput(rs))

  }

  def ascii(s: String):List[BigInt] = {
    s.toList.map(ch => BigInt(ch.toInt)) ++ List(BigInt(10))
  }

  def parseOutput(out: List[BigInt]): String = {
    out.foldLeft(""){case (s,i) => s + i.toByte.toChar}
  }

}
//#####.#.##.#.####
//#####.###########
