package aoc.y2019

import scala.util.{Try, Success, Failure}
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Queue

object Utils {

  import Control._

  def readTextFileWithTry(filename: String): Try[List[String]] = {
    Try {
        val lines = using(io.Source.fromFile(filename)) { source =>
            (for (line <- source.getLines) yield line).toList
        }
        lines
    }
  }
    def combine(n: Int, k: Int): List[List[Int]] = {
        val ans = ListBuffer[List[Int]]()
        val temp = ListBuffer[Int]()

        def dfs(start:Int,k:Int):Unit = {
            //down to bottom
            //form a possible result
            if(k == 0){
                ans += temp.toList
                //execution stream direction go up
                return
            }


            //spread out for possible results
            for(i <- start to n){
                //add current digit to current path
                temp += i

				//go further at current level
                dfs(i + 1,k - 1)

                //if down to bottom
                //back to previous statu of temp for next possible result
                temp.remove(temp.length - 1)
            }

        }

        dfs(1,k)
        ans.toList
    }
    def combineOne(n: Int, k: Int): List[List[Int]] = {
        val res = ArrayBuffer[List[Int]]()
        val queue = Queue[(List[Int], Int)]()
        (k to n).foreach(i => queue.enqueue((List(i), 1)))
        while (queue.nonEmpty) {
            val (list, i) = queue.dequeue
            if (i < k) {
                (1 until list.head).foreach(k => queue.enqueue((k +: list, i + 1)))
            }
            else {
                res += list
            }
        }
        res.toList
    }

    def time[R](block: => R): R = {
        val t0 = System.currentTimeMillis()
        val result = block    // call-by-name
        val t1 = System.currentTimeMillis()
        println("Elapsed time: " + (t1 - t0) + "ms")
        result
    }

    def main(args: Array[String]): Unit = {
        time{combineOne(26,13).length}
    }
}

object Control {
    def using[A <: { def close(): Unit }, B](resource: A)(f: A => B): B =
        try {
            f(resource)
        } finally {
            resource.close()
        }
}
