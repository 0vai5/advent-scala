package aoc.y2019

import io.Source

object Day05 {

  trait OpCode
  case class In() extends OpCode
  case class Out() extends OpCode
  case class Add() extends OpCode
  case class Mul() extends OpCode
  case class JiT() extends OpCode
  case class JiF() extends OpCode
  case class LessT() extends OpCode
  case class EQL() extends OpCode
  case class Halt() extends OpCode

  trait Mode
  case class Position() extends Mode
  case class Immediate() extends Mode

  type Operation = (OpCode,Mode,Mode,Mode)

  def operation(v: Int): Operation = {
    val s = v.toString.toList.reverse
    val op:OpCode = s.take(2) match {
      case ('1'::_) => Add()
      case ('2'::_) => Mul()
      case ('3'::_) => In()
      case ('4'::_) => Out()
      case ('5'::_) => JiT()
      case ('6'::_) => JiF()
      case ('7'::_) => LessT()
      case ('8'::_) => EQL()
      case ('9'::'9'::_) => Halt()
    }
    val params = s.drop(2)
    def mode(i: Int): Mode = {
      if (i < params.length){
        if (params(i) == '0'){
          Position()
        }else{
          Immediate()
        }
      }else{
        Position()
      }
    }
    (op,mode(0),mode(1),mode(2))
  }

  def readByMode(m: Mode, p: Int, codes: Array[Int]): Int = {
    if (m == Immediate()){
      p
    }else{
      codes(p)
    }
  }

  def writeByMode(m: Mode, p: Int, value: Int, codes: Array[Int]): Unit = {
    if(m == Immediate()){
      throw new Error()
    }else{
      codes(p) = value
    }
  }

  def solve(input: Int, codes: Array[Int]): List[Int] = {
    var pc:Int = 0;
    var output = List[Int]()
    while(true){
      val opr@(op,m1,m2,m3) = operation(codes(pc))
      op match {
        case Add() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          val v2 = readByMode(m2,codes(pc+2),codes)
          writeByMode(m3,codes(pc+3),v1+v2,codes)
          pc += 4
        case Mul() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          val v2 = readByMode(m2,codes(pc+2),codes)
          writeByMode(m3,codes(pc+3),v1*v2,codes)
          pc += 4
        case LessT() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          val v2 = readByMode(m2,codes(pc+2),codes)
          writeByMode(m3,codes(pc+3),if (v1 < v2) 1 else 0,codes)
          pc += 4
        case EQL() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          val v2 = readByMode(m2,codes(pc+2),codes)
          writeByMode(m3,codes(pc+3),if (v1 == v2) 1 else 0,codes)
          pc += 4
        case JiT() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          if(v1 != 0){
            pc = readByMode(m2,codes(pc+2),codes)
          }else {
            pc += 3
          }
        case JiF() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          if(v1 == 0){
            pc = readByMode(m2,codes(pc+2),codes)
          }else {
            pc += 3
          }
        case In() =>
          writeByMode(m1,codes(pc+1),input,codes)
          pc += 2
        case Out() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          output = v1 :: output
          pc += 2
        case Halt() =>
          pc += 1
          return output
      }
      if (pc >= codes.length)
        return output
    }
    output
  }

  def execute(input: Int, fname: String): Unit = {
    val inp = Source.fromFile(fname).getLines().next().split(',').map(_.toInt)
    println(solve(input, inp))
  }

  def test(input: Int, v: String): Unit = {
    val inp = v.split(',').map(_.toInt)
    println(solve(input, inp))
  }
}
