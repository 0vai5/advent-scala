package aoc.y2019

import io.Source

object Day06 {

  def path(os: Option[String], dict: Map[String, String], rs: List[String]=List()): List[String] = {
    os match {
      case Some(s) =>
        path(dict.get(s),dict,s::rs)
      case None => rs
    }
  }

  def part01(s:String, fname: String): List[String] = {
    val inp = Source.fromFile(fname).getLines().map(_.split(')')).map(e => (e(1),e(0)))
    val dict: Map[String,String] = inp.toMap

    path(Some(s),dict, List[String]())
  }

  def part01(fname: String): Int = {
    val inp = Source.fromFile(fname).getLines().map(_.split(')')).map(e => (e(1),e(0)))
    val dict: Map[String,String] = inp.toMap

    val rs:List[Int] = dict.keys.toList.map(k => path(Some(k),dict).length - 1)

    rs.sum
  }

  def part02(from: String, to: String, fname: String): Int = {
    val inp = Source.fromFile(fname).getLines().map(_.split(')')).map(e => (e(1),e(0)))
    val dict: Map[String,String] = inp.toMap

    val path1 = path(Some(from),dict)
    val path2 = path(Some(to),dict)

    val commonlength: Int = path1.zip(path2).takeWhile{case (a,b) => a == b}.length

    (path1.length - 1 - commonlength) + (path2.length - 1 - commonlength)
  }
}
