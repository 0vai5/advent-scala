package aoc.y2019

import scala.io.Source

object Day01 {

  def solve(f: Int => Int, fname: String): Int = {
    val lines = Source.fromFile(fname).getLines()

    lines.foldLeft(0){(r,l) =>
      r + f(l.toInt)
    }    
  }

  def fuelreq01(mass: Int): Int = mass/3 - 2

  def fuelreq02(m: Int): Int = {
    val f = fuelreq01(m)
    if (f <= 0)
      0
    else {
      f + fuelreq02(f)
    }
  }
}
