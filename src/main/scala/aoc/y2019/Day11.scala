package aoc.y2019

import scala.io.Source
import scala.collection.mutable.Map

import Day09._

object Day11 {

  type Position = (Int,Int)
  type Boundary = (Position,Position)

  trait Color{
    def code:Int
  }
  case class Black() extends Color{ def code = 0}
  case class White() extends Color{def code = 1}

  trait Direction
  case class Up() extends Direction
  case class Down() extends Direction
  case class Left() extends Direction
  case class Right() extends Direction

  case class ColorState(pos: Position, direction: Direction, colored: Map [Position, Color], bndry: Boundary)

  def runRobot(robotState: State, colorState: ColorState): (State,ColorState) = {
    val color = colorState.colored.getOrElse(colorState.pos, Black())

    val newRobotState = interprete(robotState.copy(inputs=List(color.code)).copy(output=List()))

    val (turn,newCurrcolor) = (getDirection(newRobotState.output.head), getColor(newRobotState.output.tail.head))

    val (newPos,newdrctn) = newPosition(colorState.pos, colorState.direction, turn)
    val newBndry = boundary(colorState.bndry,newPos)

    colorState.colored.put(colorState.pos,newCurrcolor)
    val newColorState = ColorState(pos=newPos,direction = newdrctn, colored = colorState.colored, bndry = newBndry)

    if(newRobotState.stopped){
      (newRobotState, newColorState)
    }else{
      runRobot(newRobotState, newColorState)
    }
  }

  def boundary(bndry: Boundary, pos: Position): Boundary = {
    val ((minX,minY),(maxX,maxY)) = bndry
    val (x,y) = pos

    val minX$ = if (x < minX) x else minX
    val minY$ = if (y < minY) y else minY
    val maxX$ = if (x > maxX) x else maxX
    val maxY$ = if (y > maxY) y else maxY
    ((minX$,minY$),(maxX$,maxY$))
  }

  def getDirection(v: BigInt): Direction = {
    v.intValue match {
      case 0 => Left()
      case 1 => Right()
    }
  }

  def getColor(v: BigInt): Color = {
    v.intValue match {
      case 0 => Black()
      case 1 => White()
    }
  }

  def newPosition(pos: Position, drctn: Direction, turn: Direction): (Position,Direction) = {
    val (x,y) = pos
    (drctn, turn) match {
      case (Up(), Left()) => ((x-1,y),Left())
      case (Up(), Right()) => ((x+1,y),Right())
      case (Down(), Left()) => ((x+1,y),Right())
      case (Down(), Right()) => ((x-1,y),Left())
      case (Left(), Left()) => ((x,y-1),Down())
      case (Left(), Right()) => ((x,y+1),Up())
      case (Right(), Left()) => ((x,y+1),Up())
      case (Right(), Right()) => ((x,y-1),Down())
    }
  }

  def execute(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = Map[BigInt,BigInt]()

    codesTuples.foreach{case (v,i) => codes(i) = v}

    val (_, colorState) = runRobot(
      State(codes=codes),
      ColorState(
        pos = (0,0),
        direction = Up(),
        colored = Map((0,0) -> White()),
        bndry = ((0,0),(0,0))))

    println(colorState.colored.size)
    println(colorState.bndry)

    val ((minX,minY),(maxX,maxY)) = colorState.bndry

    (minY - 2).to(maxY + 2).
    map(row => {
        (minX - 2).to(maxX + 2).
        map(col => {
            val rs = colorState.colored.getOrElse((col,row),Black())
            if (rs == Black())
            print(' ')
            else
            print('*')
        })
        println()
    })

  }

}
