package aoc.y2019

import scala.io.Source

import Day09._

object Day13 {

  
    // 0 is an empty tile. No game object appears in this tile.
    // 1 is a wall tile. Walls are indestructible barriers.
    // 2 is a block tile. Blocks can be broken by the ball.
    // 3 is a horizontal paddle tile. The paddle is indestructible.
    // 4 is a ball tile. The ball moves diagonally and bounces off objects.
  trait Tile
  object Tile {
    def from[T >: Tile](code: Int): T = {
      code match {
        case 0 => Empty()
        case 1 => Wall()
        case 2 => Block()
        case 3 => Horztl()
        case 4 => Ball()
      }
    }
  }
  case class Empty() extends Tile
  case class Wall() extends Tile
  case class Block() extends Tile
  case class Horztl() extends Tile
  case class Ball() extends Tile
  case class Score(score: Int) extends Tile

  def executeIntCode(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = scala.collection.mutable.Map[BigInt,BigInt]()

    codesTuples.foreach{case (v,i) => codes(i) = v}

    val state = interprete(State(codes=codes))

    val rs = state.output.reverse
    val drawn = dummyDraw(rs)

    val count =
      drawn.toList.foldLeft(0){case (c, (p,tile)) =>
      tile match {
        case Block() => c + 1
        case _ => c
      }}

    println(count)

  }

  type Position = (Int, Int)

  def dummyDraw(input: List[BigInt]): Map[Position, Tile] = {
    input.grouped(3).
      foldLeft(Map[Position,Tile]()){
        case (rsMap, x::y::z::Nil) => (x.toInt, y.toInt) match {
          case (-1, 0) => rsMap + ((-1 -> 0) -> Score(z.toInt))
          case (xx, yy) => rsMap + ((xx -> yy) -> Tile.from(z.toInt))
      }}
  }

  def executeIntCode2(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes = scala.collection.mutable.Map[BigInt,BigInt]()
    
    codesTuples.foreach{case (v,i) => codes(i) = v}
    codes(0) = 2
    val state = interprete(State(codes=codes))
    println(state.stopped)

    println(dummyPlay(state))
  }

  def dummyPlay(state: State): Option[Int] = {
    val gameState = dummyDraw(state.output.reverse)

    val (ball, hrtile, score) =
      gameState.foldLeft[(Option[Position], Option[Position], Option[Int])]((None, None, None)){
        case ((_,h,s), (p,Ball())) => (Some(p),h,s)
        case ((b,_,s), (p,Horztl())) => (b,Some(p),s)
        case ((b,h,_), (_,Score(score))) => (b,h,Some(score))
        case ((b,h,s), _) => (b,h,s)
      }

    println("Ball: " + ball)
    println("Striker : " + hrtile)
    println("Score : " + score)

    val count =
      gameState.toList.foldLeft(0){case (c, (p,tile)) =>
      tile match {
        case Block() => c + 1
        case _ => c
      }}
    println("Blocks remaining : " + count)
    println("=====================")
    println()
    println()

    if (state.stopped || count == 0){
      score
    }else{
      val (bx, hx) = (ball.get._1, hrtile.get._1)
      val direction =
        if (hx < bx){
          1
        }else if(hx > bx){
          -1
        }else{
          0
        }
      val newState = interprete(state.copy(inputs=List(direction)))
      dummyPlay(newState)
    }
  }
}
