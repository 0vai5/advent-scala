package aoc.y2019

import io.Source
import scala.collection.Set

object Day07 {

  trait OpCode
  case class In() extends OpCode
  case class Out() extends OpCode
  case class Add() extends OpCode
  case class Mul() extends OpCode
  case class JiT() extends OpCode
  case class JiF() extends OpCode
  case class LessT() extends OpCode
  case class EQL() extends OpCode
  case class Halt() extends OpCode

  trait Mode
  case class Position() extends Mode
  case class Immediate() extends Mode

  type Operation = (OpCode,Mode,Mode,Mode)

  def operation(v: Int): Operation = {
    val s = v.toString.toList.reverse
    val op:OpCode = s.take(2) match {
      case ('1'::_) => Add()
      case ('2'::_) => Mul()
      case ('3'::_) => In()
      case ('4'::_) => Out()
      case ('5'::_) => JiT()
      case ('6'::_) => JiF()
      case ('7'::_) => LessT()
      case ('8'::_) => EQL()
      case ('9'::'9'::_) => Halt()
    }
    val params = s.drop(2)
    def mode(i: Int): Mode = {
      if (i < params.length){
        if (params(i) == '0'){
          Position()
        }else{
          Immediate()
        }
      }else{
        Position()
      }
    }
    (op,mode(0),mode(1),mode(2))
  }

  def readByMode(m: Mode, p: Int, codes: Array[Int]): Int = {
    if (m == Immediate()){
      p
    }else{
      codes(p)
    }
  }

  def writeByMode(m: Mode, p: Int, value: Int, codes: Array[Int]): Unit = {
    if(m == Immediate()){
      throw new Error()
    }else{
      codes(p) = value
    }
  }

  case class State(pc: Int=0, codes: Array[Int], inputs: List[Int]=List(), output: List[Int]=List(), stopped: Boolean=false)

  def interprete(state: State): State = {
    var pc:Int = state.pc
    var output = state.output
    val codes = state.codes
    var ip = 0
    var read = false
    while(true){
      if(state.stopped){
        println("Halted!")
        return state
      }
      val opr@(op,m1,m2,m3) = operation(codes(pc))
      op match {
        case Add() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          val v2 = readByMode(m2,codes(pc+2),codes)
          writeByMode(m3,codes(pc+3),v1+v2,codes)
          pc += 4
        case Mul() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          val v2 = readByMode(m2,codes(pc+2),codes)
          writeByMode(m3,codes(pc+3),v1*v2,codes)
          pc += 4
        case LessT() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          val v2 = readByMode(m2,codes(pc+2),codes)
          writeByMode(m3,codes(pc+3),if (v1 < v2) 1 else 0,codes)
          pc += 4
        case EQL() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          val v2 = readByMode(m2,codes(pc+2),codes)
          writeByMode(m3,codes(pc+3),if (v1 == v2) 1 else 0,codes)
          pc += 4
        case JiT() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          if(v1 != 0){
            pc = readByMode(m2,codes(pc+2),codes)
          }else {
            pc += 3
          }
        case JiF() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          if(v1 == 0){
            pc = readByMode(m2,codes(pc+2),codes)
          }else {
            pc += 3
          }
        case In() =>
          if(ip >= state.inputs.length){
            return State(pc=pc,codes=codes,output=output,stopped=state.stopped)
          }else{
            writeByMode(m1,codes(pc+1),state.inputs(ip),codes)
            ip += 1
            pc += 2
            read = true
          }
        case Out() =>
          val v1 = readByMode(m1,codes(pc+1),codes)
          output = v1 :: output
          pc += 2
        case Halt() =>
          pc += 1
          return State(pc=pc,codes=codes,output=output,stopped=true)
      }
      if (pc >= codes.length)
          return State(pc=pc,codes=codes,output=output,stopped=true)
    }
    return State(pc=pc,codes=codes,output=output,stopped=true)
  }

  def loopPhaser(inputs: List[Int], codes: Array[Int]): Int  = {
    var lastOut = 0

    var sA = interprete(State(codes=codes.clone,inputs=List[Int](inputs(0))))
    var sB = interprete(State(codes=codes.clone,inputs=List[Int](inputs(1))))
    var sC = interprete(State(codes=codes.clone,inputs=List[Int](inputs(2))))
    var sD = interprete(State(codes=codes.clone,inputs=List[Int](inputs(3))))
    var sE = interprete(State(codes=codes.clone,inputs=List[Int](inputs(4))))

    println("Working for input : " + inputs)
    var maxOut = -1
    var cycle = 0
    while(true){
      //println("Executing cycle : "+cycle)
      cycle += 1
      if(sA.stopped){
        println("Error: A halted")
        return lastOut
      }
      sA = interprete(sA.copy(inputs = List(lastOut)))
      lastOut = sA.output.head

      if(sB.stopped){
        println("Error: B halted")
        return lastOut
      }
      sB = interprete(sB.copy(inputs = List(lastOut)))
      lastOut = sB.output.head

      if(sC.stopped){
        println("Error: C halted")
        return lastOut
      }
      sC = interprete(sC.copy(inputs = List(lastOut)))
      lastOut = sC.output.head

      if(sD.stopped){
        println("Error: D halted")
        return lastOut
      }
      sD = interprete(sD.copy(inputs = List(lastOut)))
      lastOut = sD.output.head

      sE = interprete(sE.copy(inputs = List(lastOut)))
      lastOut = sE.output.head
      if(maxOut < lastOut){
        maxOut = lastOut
      }
      if(sE.stopped){
        println("Sucess: E halted with last : " + lastOut + " MaxOut : " + maxOut)
        return maxOut
      }
    }
    throw new Error()
  }

  def show(arr: Array[Int]): String = {
    var s = new String()
    arr.map(i => s = s + "," + i)
    s
  }

  def applyPhase(inputs: List[Int], codes: Array[Int]): Int  = {
    var lastOut = 0
    for(input <- inputs){
      val rs = interprete(State(codes = codes.clone, inputs=List(input,lastOut)))
      lastOut = rs.output.head
    }
    println("For input : " + inputs + " Out : " + lastOut)
    lastOut
  }

  def generator(range: Range): List[List[Int]] = {
    for {
      a <- range.toList
      b <- range.toList
      c <- range.toList
      d <- range.toList
      e <- range.toList
      if (Set[Int](a,b,c,d,e).size == 5)
    } yield List[Int](a,b,c,d,e)
  }

  def execute(fname: String): Int = {
    val codes = Source.fromFile(fname).getLines().next().split(',').map(_.toInt)

    generator(0 to 4).map(inp => applyPhase(inp,codes.clone)).max
  }

  def execute02(fname: String): Int = {
    val codes = Source.fromFile(fname).getLines().next().split(',').map(_.toInt)

    generator(5 to 9).map(inp => loopPhaser(inp,codes.clone)).max
  }

  def test(input: Int, v: String): Unit = {
    val inp = v.split(',').map(_.toInt)
    //println(solve(input, inp))
  }
}
