package aoc.y2019

import io.Source

object Day02 {

  def alarm(pos: Int, codes: List[Int]): List[Int] = {
    codes.drop(pos*4).take(4) match {
      case 99::_ => codes
      case op::i1::i2::i3::Nil =>
        val func: (Int,Int) => Int = op match {
          case 1 => _ + _
          case 2 => (_ * _)
          case _ => throw new Error("")
        }
        val v1 = codes(i1)
        val v2 = codes(i2)
        val newval = func(v1,v2)
        val newcodes = codes.patch(i3,List(newval),1)
        alarm(pos + 1, newcodes)
      case _ => throw new Error("")
    }
  }

  def solve01(fname: String): List[Int] = {
    val lines = Source.fromFile(fname).getLines()

    val codes = lines.map(l => l.split(',').toList.map(_.toInt)).next()
    val newcodes = codes.patch(2,List(2),1).patch(1,List(12),1)
    alarm(0,newcodes)
  }

  def part02(reqVal: Int, codes: List[Int]): Int = {
    val replace = (n:Int, v:Int) => codes.patch(1,List(n),1).patch(2,List(v),1)

    val allNounVerbs = for {
      n <- 0 to 99
      v <- 0 to 99
    } yield (n,v)

    val (n,v) = allNounVerbs.filter(nv => alarm(0, replace(nv._1,nv._2)).head == reqVal).head
    100 * n + v
  }

  def solve02(fname: String, reqVal: Int): Int = {
    val lines = Source.fromFile(fname).getLines()

    val codes = lines.map(l => l.split(',').toList.map(_.toInt)).next()
    part02(reqVal, codes)
  }
}
