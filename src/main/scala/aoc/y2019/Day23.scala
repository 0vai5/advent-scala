package aoc.y2019

import scala.io.Source

import Day09._

object Day23 {

  type NTAdr = BigInt
  type NetworkData = Map[NTAdr, State]
  type OtherNetworkData = Map[NTAdr, List[BigInt]]
  type MutableCode = scala.collection.mutable.Map[BigInt,BigInt]

  def executeIntCode01(fname: String): Unit = {
    val codesTuples = Source.fromFile(fname).getLines().next().split(',').map(BigInt(_)).zipWithIndex

    val codes: MutableCode = scala.collection.mutable.Map[BigInt,BigInt]()
    codesTuples.foreach{case (v,i) => codes(i) = v}

    val addresses = (BigInt(0) to BigInt(49)).toList
    //init 49 pcs
    var networkData =
      addresses.foldLeft(Map[NTAdr, State]()){
        case (data, ntAddr) =>
          val inp = List(ntAddr)
          val st = State(codes = codes.clone, inputs = inp)
          data + (ntAddr -> st)
      }

    var otherNetworkData: OtherNetworkData = Map()
    var lastInputForZeroAddressFrom255 = List[BigInt]()
    var count = 1000
    while(count != 0){
      println("Count: " + count)
      count = count - 1

      val result: List[(NTAdr, State, List[BigInt], List[BigInt])] =
        for {
          addr <- addresses
          state <- networkData.get(addr)
          val newState = state.inputs match {
            case Nil => interprete(state.copy(inputs  = List(-1)).copy(output = Nil))
            case _ => interprete(state.copy(output = Nil))
          }
        } yield (addr, newState, newState.output.reverse, state.inputs)

      //first build networkData with the new states
      val newNetworkData: NetworkData = result.map{ case (adr, state, _, _) => (adr,state) }.toMap

      //now send output packets to inputs in the newNetworkData
      val (networkDataWithNewInputs,newOtherNetworkData) = result.foldLeft((newNetworkData,otherNetworkData)){
        case (data, (_, _, output,_)) =>
          output.grouped(3).toList.foldLeft(data){
            case ((data,otherData), ntAdr::x::y::Nil) =>
              //println(ntAdr, "=> ",x,y)
              data.get(ntAdr) match {
                case None =>
                  //this address must "not" be 0 to 49
                  //as of now i do not remember the previous inputs sent to
                  //these addresses. note this address do not get executed
                  //and the inputs are overridden with new value
                  //this is the required behavior for 255 in part 2 but as of now this is
                  //default for all such addresses
                  println("Address " + ntAdr + " not found! Creating new state with inputs: ", x , y)
                  val inp = List(x, y)
                  (data, otherData + (ntAdr -> inp))
                case Some(st) =>
                  val inp = st.inputs ++ List(x,y)
                  val newSt = st.copy(inputs = inp)
                  (data + (ntAdr -> newSt), otherData)
              }
          }
      }

      //part2
      //check if no input sent and no output came from the network 0 to 49
      networkData = (result.find{case (_,_,output,_) => output.size > 0}, result.find{case (_,_,_, input) => input.size > 0}) match {
        case (None,None) =>
          //network is idle
          println("Network is idle")
          newOtherNetworkData.get(255) match {
            case None => println("Error, nothing to be sent from 255 on idle network"); networkDataWithNewInputs
            case Some(inp) =>
              println("Sending " + inp + " to address 0 from 255");
              if(lastInputForZeroAddressFrom255 == inp){
                println("########### Duplicate input sent ......" + inp)
                count = 0
              }
              lastInputForZeroAddressFrom255 = inp
              networkDataWithNewInputs.get(0).map{ case st => networkDataWithNewInputs + (BigInt(0) -> st.copy(inputs = inp)) }.get
          }
        case _ => networkDataWithNewInputs
      }
      otherNetworkData = newOtherNetworkData
    }

    println(networkData.get(255).map(_.inputs.take(10)))
    println(networkData.size)
  }
}
